/**
 * @file imenhance.cpp
 * @author hui han chin
 * @date 03/2014
 */

#include "imenhance.hpp"

void estimate_shadow(Mat image_base, Mat image_shadow);
void estimate_base_params(Mat image_base, Mat image_shadow, double base_params[4]);
void base_adjust(Mat image_base, Mat image_base_adj);
void detail_adjust(Mat image_base, Mat image_base_adj);

/* code from http://www.morethantechnical.com/2011/11/13/just-a-simple-laplacian-pyramid-blender-using-opencv-wcode */
class LaplacianBlending 
{
    private:
        Mat left;
        Mat right;
        Mat blendMask;
        vector<Mat> leftLapPyr,rightLapPyr,resultLapPyr;
        Mat leftSmallestLevel, rightSmallestLevel, resultSmallestLevel;
        vector<Mat> maskGaussianPyramid; //masks are 3-channels for easier multiplication with RGB
        unsigned int levels;

        void buildPyramids() {
            buildLaplacianPyramid(left,leftLapPyr,leftSmallestLevel);
            buildLaplacianPyramid(right,rightLapPyr,rightSmallestLevel);
            buildGaussianPyramid();
        }

        void buildGaussianPyramid() {
            assert(leftLapPyr.size()>0);
            maskGaussianPyramid.clear();
            Mat currentImg;
            blendMask.copyTo(currentImg);

            maskGaussianPyramid.push_back(currentImg); //highest level
            currentImg = blendMask;

            for (unsigned int l=1; l<levels+1; l++) {
                Mat _down; 
                if (leftLapPyr.size() > l) {
                    pyrDown(currentImg, _down, leftLapPyr[l].size());
                } else {
                    pyrDown(currentImg, _down, leftSmallestLevel.size()); //smallest level
                }

                Mat down; 
                _down.copyTo(down);
                maskGaussianPyramid.push_back(down);
                currentImg = _down;
            }
        }

        void buildLaplacianPyramid(const Mat& img, vector<Mat>& lapPyr, Mat& smallestLevel) {
            lapPyr.clear();
            Mat currentImg = img;
            for (unsigned int l=0; l<levels; l++) {
                Mat down,up;
                pyrDown(currentImg, down);
                pyrUp(down, up, currentImg.size());
                Mat lap = currentImg - up;
                lapPyr.push_back(lap);
                currentImg = down;
            }
            currentImg.copyTo(smallestLevel);
        }

        Mat reconstructImgFromLapPyramid() {
            Mat currentImg = resultSmallestLevel;
            for (int l=levels-1; l>=0; l--) {
                Mat up;

                pyrUp(currentImg, up, resultLapPyr[l].size());
                currentImg = up + resultLapPyr[l];
            }
            return currentImg;
        }

        void blendLapPyrs() {
            resultSmallestLevel = leftSmallestLevel.mul(maskGaussianPyramid.back()) + 
                rightSmallestLevel.mul(Scalar(1.0,1.0,1.0) - maskGaussianPyramid.back());
            cout<<"going through levels" << endl;
            for (unsigned int l=0; l<levels; l++) {
                Mat A = leftLapPyr[l].mul(maskGaussianPyramid[l]);
                Mat antiMask = Scalar(1.0,1.0,1.0) - maskGaussianPyramid[l];
                Mat B = rightLapPyr[l].mul(antiMask);
                Mat blendedLevel = A + B;

                resultLapPyr.push_back(blendedLevel);
            }
        }

    public:
        LaplacianBlending(const Mat& _left, const Mat& _right, const Mat& _blendMask, int _levels):
            left(_left),right(_right),blendMask(_blendMask),levels(_levels) 
    {
        assert(_left.size() == _right.size());
        assert(_left.size() == _blendMask.size());
        cout<< "in blender" <<endl;
        buildPyramids();
        cout << "built pyramids" << endl;
        blendLapPyrs();
        cout << "blend pyramids" << endl;
    };

        Mat blend() {
            return reconstructImgFromLapPyramid();
        }   
};

Mat LaplacianBlend(const Mat& l, const Mat& r, const Mat& m) {
    LaplacianBlending lb(l,r,m,4);
    return lb.blend();
}

int main(int argc, char** argv)
{

    /* input checks*/
    try
    {
        po::options_description pie_opt("allowed options");
        pie_opt.add_options()
            ("help","usage")
            ("lambda", po::value<double>(),"tvd lambda" )
            ("input", po::value<string>(),"input file")
            ("output", po::value<string>(),"output file")
            ("darkPt", po::value<double>(),"shadow point")
            ("darkAlpha", po::value<double>(),"shadow relight mult")
            ("darkBeta", po::value<double>(),"shadow relight additive")
            ;

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, pie_opt), vm);
        po::notify(vm);

        /* parse the opts */
        if(vm.count("help"))
        {
            cout << pie_opt <<endl;
            exit(EXIT_SUCCESS);
        }

        if(vm.count("lambda"))
            tvd_lambda = vm["lambda"].as<double>();
        if(vm.count("input"))
            input_filename = vm["input"].as<string>();
        if(vm.count("output"))
            output_filename = vm["output"].as<string>();
        if(vm.count("darkPt"))
            dark_pt = vm["darkPt"].as<double>();
        if(vm.count("darkAlpha"))
            dark_alpha = vm["darkAlpha"].as<double>();
        if(vm.count("darkBeta"))
            dark_beta = vm["darkBeta"].as<double>();

    }
    catch(exception& e)
    {
        cerr << "option error: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }




    //Panchro for now
    image_source = imread(input_filename,  IMREAD_GRAYSCALE);

    /* base+detail decomposition*/
    vector<Mat> test;
    test.push_back(image_source);
    //the denoise_TVL1 needs uint8 and lambda in 0 to 2
    optim::denoise_TVL1(test, image_base, tvd_lambda, tvd_niters);
    image_base.convertTo(image_base, CV_16S);
    image_source.convertTo(image_source, CV_16S);
    subtract(image_source, image_base, image_detail); 

    /*extract the shadow mask*/
    image_base.convertTo(image_shadow, CV_8UC1);
    threshold(image_shadow, image_shadow, dark_pt, 255, THRESH_BINARY);
    int morph_elem = 0;
    int morph_size = 2;
    Mat element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
    morphologyEx( image_shadow, image_shadow, MORPH_CLOSE, element );
    morphologyEx( image_shadow, image_shadow, MORPH_OPEN, element );

    /* base layer relight */
    image_base.convertTo(image_base_adj, -1, dark_alpha, dark_beta);
    image_base_adj.convertTo(image_base_adj, CV_8UC1); 
    image_base.convertTo(image_base, CV_8UC1);




    /* detail layer adjustment */
    image_detail.convertTo(image_detail_adj, -1, 1.5, 0);

    image_detail_adj.convertTo(image_detail_adj, CV_8UC1);
    /* poisson edge blending */

    /* recombing the images */
    // image_base.convertTo(image_base, CV_64F);
    // image_base_adj.convertTo(image_base_adj, CV_64F);
    add(image_base_adj, image_detail_adj, image_enhance);
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(4);
    clahe->apply(image_enhance, image_enhance);

    image_source.convertTo(image_source, CV_8UC1);
    Mat image_enhance_temp;
    Mat image_source_temp;
    Mat image_nonshadow;
    bitwise_not(image_shadow, image_nonshadow);
    image_enhance.copyTo(image_enhance_temp, image_nonshadow);
    image_source.copyTo(image_source_temp, image_shadow);


    image_source.convertTo(image_source, CV_8UC1);

    image_source_temp.convertTo(image_source_temp, CV_32F, 1.0/255.0);
    image_enhance_temp.convertTo(image_enhance_temp, CV_32F, 1.0/255.0);
    image_shadow.convertTo(image_shadow, CV_32F, 1.0/255.0);

    image_enhance = LaplacianBlend(image_source_temp, image_enhance_temp, image_shadow);
    normalize(image_enhance, image_enhance, 0, 255, NORM_MINMAX, CV_8UC1);
    

    //  normalize(image_enhance, image_enhance,0, 255, NORM_MINMAX,CV_8UC1);
    imshow("0", image_source);
    imshow("1", image_enhance);
    waitKey(0);
    /* write out the result */
    //  imwrite("base.tiff", image_base);                
    // imwrite("detail.tiff", image_detail);                

    // imwrite(output_filename, image_enhance);


    /* clean up */
    image_temp.release();
    image_source.release();
    image_shadow.release();
    image_base.release();
    image_base_adj.release();
    image_detail.release();
    image_detail_adj.release();
    image_enhance.release();
    exit(EXIT_SUCCESS);
}
