CC=g++
INCLUDES=-I/usr/local/include/opencv -I/usr/local/include -I/usr/local/cuda-5.5 -I/usr/local/include/boost
CXXFLAGS=-g -Wall 
LDFLAGS=
LIBS=`pkg-config --cflags --libs opencv` -L/usr/local/cuda-5.5/lib64/ -L/usr/local/lib/ -lboost_program_options

MAIN=imenhance
SRCS=$(MAIN).cpp $(MAIN).hpp
OBJS=$(SRCS:.cpp=.o)


.PHONY: depend clean

all: $(MAIN) 


$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@


clean:
	$(RM) *.o *~ $(MAIN)


