/** @file imenhance_hpp
 *  @author hui han chin, 03/2014
 */
#ifndef IMENHANCE_HPP
#define IMENHANCE_HPP


/** includes **/
/* standard */
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <cmath>
#include <unistd.h>
#include <iterator>

/* opencv*/
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/optim/optim.hpp"

/* boost */
#include <boost/program_options.hpp>

/* namespace */
namespace po = boost::program_options;
using namespace std;
using namespace cv;


void estimate_shadow(Mat image_base, Mat image_shadow);
void estimate_base_params(Mat image_base, Mat image_shadow, double base_params[4]);
void base_adjust(Mat image_base, Mat image_base_adj);
void detail_adjust(Mat image_base, Mat image_base_adj);



/** global variables **/

string input_filename;
string output_filename = "out.tiff";

/* for the enhancement algorithm */
Mat image_temp;
Mat image_source;
Mat image_base;
Mat image_detail;
Mat image_shadow;
Mat image_base_adj;
Mat image_detail_adj;
Mat image_enhance;

/* base layer params */
double tvd_lambda = 1;
int tvd_niters = 100;
double dark_pt = 50;
double dark_alpha = 1.1;
double dark_beta = 10;
#endif
